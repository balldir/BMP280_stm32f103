# BMP280_stm32f103

Bringup of BMP280 for maple like stm32f103 dev board using semihosting

Connect connect board to debugger, SDA to PB7, SCL to PB6 and run.

```
make flash
```

Requires openocd and arm-none-eabi to work