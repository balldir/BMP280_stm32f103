#include "stm32f1xx_hal.h"
#include "i2c.h"
#include "gpio.h"
#include "bmp280.h"

/*Define some stack*/
#define STACK_SIZE 2048
/*Looks ugly. Probably need a macro*/
__attribute__((section(".stack"))) __attribute__((used)) static uint8_t stack[STACK_SIZE]  = {};

#define BMP280_ADDR1 0b1110110
#define BMP280_ADDR2 0b1110111
#define TIMEOUT_MS   200
#define BMP280_REG_ID 0xD0 

static s8  BMP280_I2C_bus_read(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt)
{
	return -(!!HAL_I2C_Mem_Read(&hi2c1, dev_addr << 1,  reg_addr, I2C_MEMADD_SIZE_8BIT, reg_data, cnt, TIMEOUT_MS));
}

static s8  BMP280_I2C_bus_write(u8 dev_addr, u8 reg_addr, u8 *reg_data, u8 cnt)
{
	return -(!!HAL_I2C_Mem_Write(&hi2c1, dev_addr << 1,  reg_addr, I2C_MEMADD_SIZE_8BIT, reg_data, cnt, TIMEOUT_MS));
}

static void BMP280_delay_msec(BMP280_MDELAY_DATA_TYPE ms)
{
	HAL_Delay(ms);
}

void user_function()
{
	printf(" > Started\n");
	const uint8_t addr_array[] = {BMP280_ADDR1, BMP280_ADDR2};
	struct bmp280_t bmp280;
	bmp280.bus_read = BMP280_I2C_bus_read;
	bmp280.bus_write = BMP280_I2C_bus_write;
	bmp280.delay_msec = BMP280_delay_msec;
	for (size_t i = 0; i < ( sizeof(addr_array)/sizeof(addr_array[1]) ) ; i += 1) {
		bmp280.chip_id = 0xFF;
		bmp280.dev_addr = addr_array[i];
		s32 com_rslt = bmp280_init(&bmp280);
		printf("  > Testing 0x%2X -> %s\n", bmp280.dev_addr, (com_rslt == 0) ? "Found" : "Not found");
		if (com_rslt != 0) {
			continue;
	      }
	      /* CP from template */
	      /* The variable used to assign the standby time*/
	      u8 v_standby_time_u8 = BMP280_INIT_VALUE;
	      /* The variable used to read uncompensated temperature*/
	      s32 v_data_uncomp_tem_s32 = BMP280_INIT_VALUE;
	      /* The variable used to read uncompensated pressure*/
	      s32 v_data_uncomp_pres_s32 = BMP280_INIT_VALUE;
	      /* The variable used to read real temperature*/
	      s32 v_actual_temp_s32 = BMP280_INIT_VALUE;
	      /* The variable used to read real pressure*/
	      u32 v_actual_press_u32 = BMP280_INIT_VALUE;
	      s32 v_actual_press_data_s32 = BMP280_INIT_VALUE;
	      /* result of communication results*/

	      com_rslt += bmp280_set_power_mode(BMP280_FORCED_MODE);
	      com_rslt += bmp280_set_work_mode(BMP280_HIGH_RESOLUTION_MODE);
	      com_rslt += bmp280_set_standby_durn(BMP280_STANDBY_TIME_1_MS);
	      com_rslt += bmp280_read_pressure_temperature(&v_actual_press_u32,
	      &v_actual_temp_s32);
	      if (com_rslt != 0) {
		      continue;
	      }
	      printf("   > Temp %i.%.2i C preassure  %i.%.2i millibar", v_actual_temp_s32/100, v_actual_temp_s32 % 100 , v_actual_press_u32/100, v_actual_press_u32 % 100);
	}
}